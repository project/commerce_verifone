<?php

namespace Drupal\commerce_verifone\Controller;

use Drupal\commerce_verifone\DependencyInjection\KeysHelper;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Verifone\Core\DependencyInjection\CryptUtils\RsaKeyGenerator;

class AdminController extends ControllerBase {

  /**
   * Returns a render-able array for a test page.
   */
  public function generateKeys(Request $request) {
    $success = TRUE;
    $messages = [];

    $type = $request->request->get('keys_type');
    $id = $request->request->get('id');

    if (empty($type) || $type === FALSE) {
      $success = FALSE;
      $messages[] = $this->t('Problem with generating new keys.') . $this->t('Please refresh the page and try again.');
    }

    if (!$success) {
      return new JsonResponse(['success' => $success, 'messages' => $messages]);
    }

    $generator = new RsaKeyGenerator();
    $resultGenerate = $generator->generate();

    if ($resultGenerate) {
      $helper = new KeysHelper();
      $resultStoreKey = $helper->storeKeys($id, $type, $generator->getPublicKey(), $generator->getPrivateKey());

      if ($resultStoreKey === TRUE) {
        $messages[] = $this->t('Keys are generated correctly. Please refresh or save the configuration.');
        return new JsonResponse(['success' => $success, 'messages' => $messages]);
      }

      $success = FALSE;
      $messages[] = $resultStoreKey;

    }
    else {
      $success = FALSE;
      $messages[] = $this->t('Problem with generating new keys.');
    }

    $response['type'] = $request->request->get('keys_type');

    $response['data'] = 'Not implemented yet!';
    $response['method'] = 'POST';

    return new JsonResponse(['success' => $success, 'messages' => $messages]);
  }

}
